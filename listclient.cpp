//--- Program to test List class.

#include <iostream>
using namespace std;

#include "list.h"
#include <cassert>
#include <cstdlib>		//random numbers
#include <ctime>		//time function


int testMerge() {
   List L1, L2, L3, T;

   for (int i=0; i<5; i++) {
      int r = rand()%20;
      int s = rand()%20;
      L1.insert(r, i);
      L2.insert(s, i);
      T.insert(r, i*2); T.insert(s, i*2+1);
   }

   T.sort();
   L1.sort();
   L2.sort();
   L3 = L1.merge(L2);
   assert(L3 == T);
   cout << "TestMerge passed!" << endl;
}



int testCount() {
   List L1;
   int ctr = 0;
   int s = rand()%10;
   for (int i=0; i<20; i++) {
      int r = rand()%10;
      L1.insert(r, i);
      if (s==r) ctr++;
   }
   assert(L1.count(s) == ctr);
   cout << "testCount passed!" << endl;
}



// We create two lists L1, L2. That contain exactly the same
// elements except that L1 contains one less element.
// We pop the element from L2 and compare against L1.

int testPop(bool withParam) {
   List L1, L2;
   ElementType popedVal;

   cout << "Testing the pop ...." << endl;

   int pos;
   if (withParam)
      pos = rand()%10; // position to pop
   else
      pos = 9;

   for (int i=0; i<10; i++) {
      int r = rand()%100;
      L2.push(r);
      // insert everything to L1 except the element that
      // will be at position pos.
      if (pos!=i)
         L1.push(r);
      else
         popedVal = r;
   }
   cout << "L2 contains: " << L2 << endl;
   assert(popedVal == L2.pop(pos));
   cout << "After poping the " << pos << "th element contains: " << L2 << endl;
   assert(L1 == L2);

   cout << "Passed the test!" << endl;

}

void testStride01() {
    int A[] = {10, 20, 30, 40, 50, 60};
    int R[] = {10, 30, 50, 20, 40, 60};
    List L, M;

    for (int i=5; i >= 0; i--) {
        L.push(A[i]);
        M.push(R[i]);
    }

    assert(L.stride(2) == M);
    cout << "Passed the testStride01!!" << endl;
}

//Long main again
int main()
{
    srand(time(NULL));

    List L, Practice_Mode1, Practice_Mode2, Practice_Mode3;
	List Practice_Mode4, Practice_Mode5, Practice_Mode6;

    for(int c = 0; c < 10; c++)
		L.push(rand() % 10);

    //cout << "I am your client, feed me!" << endl;
    //testMerge();
    //testCount();
    // testPop(true);


    testStride01();

	int A1[] = {1, 3, 4, 6, 8, 5, 3};
	int A2[] = {1, 3, 4, 6, 8, 5, 7};
	int A3[] = {1, 3, 3, 5, 8, 5, 7};

	for(int c = 0; c < 7; c++)
	{
		Practice_Mode1.push(A1[c]);
		Practice_Mode2.push(A2[c]);
		Practice_Mode3.push(A3[c]);
	}

	int A4[] = {1, 3, 1, 3, 5, 5};
	int A5[] = {1, 1, 1, 1, 1, 5};
	int A6[] = {1, 1, 1, 1, 1, 1};

	for(int c = 0; c < 6; c++)
	{
		Practice_Mode4.push(A4[c]);
		Practice_Mode5.push(A5[c]);
		Practice_Mode6.push(A6[c]);
	}


    cout << endl << "Now to test elimOdds..." << endl << endl
		 << "List L: " << L << endl
		 << "List L without odds: " << L.elimOdds() << endl << endl
		 << "Now to test mode..." << endl << endl
		 << "Practice Mode 1 : " << Practice_Mode1 << endl
		 << "Its Mode: " << Practice_Mode1.mode() << endl << endl
		 << "Practice Mode 2 : " << Practice_Mode2 << endl
		 << "Its Mode: " << Practice_Mode2.mode() << endl << endl
		 << "Practice Mode 3 : " << Practice_Mode3 << endl
		 << "Its Mode: " << Practice_Mode3.mode() << endl << endl
		 << "Practice Mode 4 : " << Practice_Mode4 << endl
		 << "Its Mode: " << Practice_Mode4.mode() << endl << endl
		 << "Practice Mode 5 : " << Practice_Mode5 << endl
		 << "Its Mode: " << Practice_Mode5.mode() << endl << endl
		 << "Practice Mode 6 : " << Practice_Mode6 << endl
		 << "Its Mode: " << Practice_Mode6.mode() << endl << endl;
	
	
    return 0;
}
