/*-- List.cpp------------------------------------------------------------

   This file implements List member functions.

-------------------------------------------------------------------------*/

#include <cassert>
#include <cstdlib>
using namespace std;

#include "list.h"





//--- Definition of class constructor
List::List()
: mySize(0)
{}

//--- Definition of empty()
bool List::empty() const
{
   return mySize == 0;
}

//--- Definition of display()
void List::display(ostream & out) const
{
	if(mySize == 0)
	{
		out << "Empty List";
		return;
	}
	
   for (int i = 0; i < mySize; i++)
     out << myArray[i] << "  ";
}

//--- Definition of output operator
ostream & operator<< (ostream & out, const List & aList)
{
   aList.display(out);
   return out;
}

//--- Definition of insert()
void List::insert(ElementType item, int pos)
{
   if (mySize == CAPACITY)
   {
      cerr << "*** No space for list element -- terminating "
              "execution ***\n";
      exit(1);
   }
   if (pos < 0 || pos > mySize)
   {
      cerr << "*** Illegal location to insert -- " << pos
           << ".  List unchanged. ***\n";
      return;
   }

   // First shift array elements right to make room for item

   for(int i = mySize; i > pos; i--)
      myArray[i] = myArray[i - 1];

   // Now insert item at position pos and increase list size
   myArray[pos] = item;
   mySize++;
}

//--- Definition of erase()
void List::erase(int pos)
{
   if (mySize == 0)
   {
      cerr << "*** List is empty ***\n";
      return;
   }
   if (pos < 0 || pos >= mySize)
   {
      cerr << "Illegal location to delete -- " << pos
           << ".  List unchanged. ***\n";
      return;
   }

   // Shift array elements left to close the gap
   for(int i = pos; i < mySize; i++)
       myArray[i] = myArray[i + 1];

   // Decrease list size
    mySize--;
}



void List::push(ElementType e)  {
  insert(e,mySize);
}


void List::sort() {
  // Step through each element of the array
  for (int nStartIndex = 0; nStartIndex < mySize; nStartIndex++)
  {
      // nSmallestIndex is the index of the smallest element
      // we've encountered so far.
      int nSmallestIndex = nStartIndex;

      // Search through every element starting at nStartIndex+1
      for (int nCurrentIndex = nStartIndex + 1; nCurrentIndex < mySize; nCurrentIndex++)
      {
          // If the current element is smaller than our previously found smallest
          if (myArray[nCurrentIndex] < myArray[nSmallestIndex])
              // Store the index in nSmallestIndex
              nSmallestIndex = nCurrentIndex;
      }

      // Swap our start element with our smallest element
      swap(myArray[nStartIndex], myArray[nSmallestIndex]);
  }
}

bool List::operator==(const List& L) const {
  if (mySize != L.mySize) return false;
  for (int i=0; i<mySize; i++) {
    //cout << myArray[i] << " " << L.myArray[i] << endl;
    if (myArray[i] != L.myArray[i]) return false;
  }
  return true;
}


int List::count(ElementType e)
{
	int amount = 0;

	for(int c = 0; c < mySize; c++)
		if(e == myArray[c])
			amount++;

	return amount;
}

ElementType List::pop(int pos)
{
	if (pos < 0 || pos >= mySize)
   {
      cerr << "Illegal location to delete -- " << pos
           << ".  List unchanged. ***\n";
      exit(1);
   }

	ElementType value = myArray[pos];

	this->erase(pos);

	return value;
}

ElementType List::pop()
{
	ElementType value = myArray[mySize - 1];

	this->erase(mySize - 1);

	return value;
}

void List::operator = (List L)
{
	for(int c = 0; c < mySize; c++)
		this->erase(0);

	for(int c = 0; c < L.mySize; c++)
		this->insert(L.myArray[c], c);
}

List List::merge(List& R)
{
	List First, Last, Merged;
	if(this->empty() and R.empty()) return Merged;

	First = *this;
	Last = R;


	while(!First.empty() or !Last.empty())
	{
		if(First.empty())
		{
			for(int c = 0; c < Last.mySize; c++)
				Merged.push(Last.pop(0));

			break;
		}

		if(Last.empty())
		{
			for(int c = 0; c < First.mySize; c++)
				Merged.push(First.pop(0));

			break;
		}

		if(First.myArray[0] < Last.myArray[0])
			Merged.push(First.pop(0));

		else
			Merged.push(Last.pop(0));
	}

	return Merged;
}

//Stride Function
List List::stride(int m)
{
	List R;		//returning list
	int counter = 0;

	//checks if the integer is a divisor
	if((mySize % m != 0) or (m < 0)) return R;
	
	
	//inserts the elements using the size as a modulus
	//substracts the List's size minus one to get all the elements
	for(int i = 0; i < mySize; i++) {
		if(counter >= mySize) counter -= (mySize - 1);
		R.push(myArray[counter % mySize]);
		counter += m;
	}

	return R;
}

//elimOdds from a given list and returns it
List List::elimOdds()
{
	List R;
	
	for(int i = 0; i < mySize; i++)
		if(myArray[i] % 2 == 0)
			R.push(myArray[i]);
	
	return R;
}

//Mode function for Lists
List List::mode()
{
	List R;	//returning list
	
	//parallel array such that each position indicates how many
	//elements are in the original List
	SweetArray CountDuplicates(mySize);
	
	//array that will contain all the unique elements
	SweetArray UniqueElements(mySize);
	int modeMax, uniqueSize;
	
	for(int c = 0; c < mySize; c++)
	{
		CountDuplicates[c] = 1;	//assumes every element appears once
		UniqueElements[c] = myArray[c];
	}
	
	//counts the repeated elements
	for(int i = 0; i < mySize - 1; i++)
		for(int j = i + 1; j < mySize; j++)
			if(myArray[i] == myArray[j])
				CountDuplicates[i]++;
	
	UniqueElements.removeDuplicates();		
	uniqueSize = UniqueElements.getSize();
	
	modeMax = CountDuplicates.getMax();

	//Either they're all unique or they're all the same element
	if((modeMax == mySize) or (modeMax == 1)) return R;
	
	//if the amount of the mode times the unique elements
	//equal the size, then all the elements are repeated the same
	if(uniqueSize * modeMax == mySize) return R;
	
	for(int c = 0; c < mySize; c++)
		if(modeMax == CountDuplicates[c])
			R.push(myArray[c]);
	
	return R;
}
				
				