#include <iostream>
#include "sweetArray.h"
using namespace std;

SweetArray::SweetArray() {
	array = new int[10];
	size = 10;

	for(int c = 0; c < size; c++)
		array[c] = 0;
}
SweetArray::SweetArray(int n) {
	if(n < 1)
	{
		size = 0;
		array = NULL;
	}
	
	else
	{
		array = new int[n];
		size = n;

		for(int c = 0; c < size; c++)
			array[c] = 0;
	}
}

SweetArray::~SweetArray()
{
	delete [] array;
	array = NULL;
}

int& SweetArray::operator[](int i) {
	return array[i];
}

void SweetArray::operator = (SweetArray F)
{
	delete [] array;

	size = F.size;

	array = new int[size];

	for(int c = 0; c < size; c++)
		array[c] = F.array[c];
}

void SweetArray::sort()
{
	if(size == 1) return;

	int minimum, pos, swapper;

	for(int i = 0; i < size - 1; i++)
	{
		minimum = array[i];
		pos = i;

		for(int c = i + 1; c < size; c++)
			if(minimum > array[c])
			{
				minimum = array[c];
				pos = c;
			}

		swapper = array[i];
		array[i] = minimum;
		array[pos] = swapper;
	}
}

void SweetArray::removeDuplicates()
{
	bool Duplicates[size];
	int actual_size = size, pos = 0;
	
	for(int c = 0; c < size; c++)
		Duplicates[size] = false;
	
	for(int c = 0; c < (size - 1); c++)
		for(int i = c + 1; i < size; i++)
			if((array[c] == array[i]) and !Duplicates[c])
			{
				Duplicates[i] = true;
				actual_size--;
			}
	
	SweetArray sweet(actual_size);
	
	for(int c = 0; c < size; c++)
		if(!Duplicates[c])
			sweet.array[pos++] = array[c];
	
	*this = sweet;
}		
		
ostream& operator<<(ostream& out, SweetArray& S) {
	if (S.size == 0)
	{
		cout << "NULL";
		return out;
	}
	out << S.array[0];
	for (int i = 1; i < S.size; i++)
		out << ", " << S.array[i];
	return out;
}

SweetArray SweetArray::operator + (SweetArray& R)
{
	SweetArray sweet(R.size);

	for(int c = 0; c < size; c++)
		sweet.array[c] = array[c] + R[c];

	return sweet;
}

SweetArray SweetArray::operator & (SweetArray& R)
{
	SweetArray sweet(size + R.size);
	int actual_size = 0;
	
	for(int c = 0; c < size; c++)
		for(int i = 0; i < (R.size); i++)
			if(array[c] == R.array[i])
			{
				sweet.array[actual_size++] = R[i];
				break;
			}

	sweet.size = actual_size;

	sweet.removeDuplicates();
	
	sweet.sort();
	
	return sweet;
}

SweetArray SweetArray::operator | (SweetArray& R)
{
	SweetArray Union(size + R.size);
	int pos = 0;
	
	for(int c = 0; c < size; c++)
		Union.array[pos++] = array[c];
	
	for(int c = 0; c < R.size; c++)
		Union.array[pos++] = R[c];
	
	Union.removeDuplicates();
	
	Union.sort();

	return Union;
}

int SweetArray::getMax()
{
	int maximum = array[0];
	
	for(int c = 0; c < size; c++)
		if(maximum < array[c])
			maximum = array[c];
	
	return maximum;
}

int SweetArray::getSize()
{
	return size;
}