/*=============================================================================
Name: 		Eduardo Ortiz
e-mail:		eduardo3991@hotmail.com
=============================================================================*/

#ifndef SWEETARRAY
#define SWEETARRAY

#include <iostream>
using namespace std;
class SweetArray {
private:	
	int *array;
	int size;
public:
	SweetArray();
	SweetArray(int n);
	~SweetArray();
	int& operator[](int i);
	void operator = (SweetArray);
	void sort();
	void removeDuplicates();
	int getMax();
	int getSize();

friend ostream& operator<<(ostream& out, SweetArray& S);

	SweetArray operator + (SweetArray&);	//sum of each element
	
	SweetArray operator & (SweetArray&);	//Intersection of sets
	
	SweetArray operator | (SweetArray&);	//Union of sets

};

#endif